# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 3
#
# This program uses strings, iterations, and etc to implement an encryption algorithm
# called Solitaire. The algorithm is implemented using a deck of cards with a total
# of 54 cards with numbers 1 and 2 as joker. The combination of cards, generated from
# user message are stored into a string.
#
# This program will have the ability to encrypt and decrypt messages.

__author__ = 'Ben'

from types import *

deck = ''
mode = ''
message = ''


def user_input():
    """
    This function acts to take in user parameters.
    """
    global mode, message, deck

    while True:
        raw_data = input('Encrypt (e) or Decrypt (d)? ')
        if raw_data not in 'ed':
            print('Invalid Input: {0}. Choose between letters "e" or "d".\n'.format(raw_data))
            continue
        if (raw_data in 'ed') and (raw_data not in '\n'):
            mode = raw_data
            break
    while True:
        raw_data = input('What is your message? ')
        try:
            user_data = str(raw_data)
        except ValueError:
            print('Cannot parse input into string: {0}'.format(raw_data))
            continue
        if user_data not in '\n':
            message = user_data
            break
    while True:
        raw_data = input('What is the initial deck? ')
        try:
            user_data = str(raw_data)
        except ValueError:
            print('Cannot parse input into string: {0}'.format(raw_data))
            continue
        if user_data not in '\n':
            deck = user_data
            break


def character_to_value(character):
    """
    This function returns the numerical placement of where the letter is in the alphabet.
    ord() converts symbols and letters into unicode and the value 'a' in particular is the
    integer value 97. Since this function is only meant to convert letters of the alphabet
    to a numerical representation so we want 'a' to start at 1. The way to do this is to
    take the unicode number for 'a' and subtracted from our converted letter passed into
    the function. We then must offset it by 1 so that 'a' starts at 1 and not 0. The lower
    case and upper case letters have different unicode values so the upper case is
    distinguished by having the the upper case numerical representation come after the
    lower case numerical representation of the alphabet, i.e. 'A' = 27.

    WARNING: Only works with strings of one character and unicode characters.
    """
    if len(character) > 1:
        print('Error: Input has more than one character. Your input: {0}'.format(character))
        exit(-1)

    offset = 1
    if character in 'abcdefghijklmnopqrstuvwxyz':
        return (ord(character) - ord('a')) + offset
    elif character in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
        uppercase_offset = 26
        return (ord(character) - ord('A')) + uppercase_offset + offset
    elif character in '12':
        # This part of the function handles the jokers which are represented as 1 or
        # 2 and is transformed to their numerical representation 53 and 54
        if character == '1':
            return 53
        if character == '2':
            return 54
    else:
        # If it falls into none of the above, it will simply return what was passed in.
        return character


def value_to_character(value):
    """
    This function returns the alphabetical representation of the number. ord() converts
    numbers between 1 and 54 into alphabetical values. This function can be considered
    the inverse of the character_to_value() function.

    WARNING: Only works with values of type int. Do not input values of any other type.
    Range of int input: value > 0
    """
    if type(value) is IntType:
        offset = -1
        modded_value = (value + offset) % 52
        if modded_value in range(0, 26):
            return unichr(modded_value + ord('a'))
        elif modded_value in range(26, 52):
            uppercase_offset = -26
            return unichr(modded_value + ord('A') + uppercase_offset)
    else:
        # If it falls into none of the above, it will simply return what was passed in.
        return value


def encrypt_character(character, card_deck):
    """
    Takes in a character and the deck and encrypts the character.
    It first retrieves a character/value from the deck, converts that character/value
    and then adds that value to the numerical representation of the character passed in.
    The new number is then converted back into a character and returned.
    """
    generated_character = retrieve_key(card_deck)
    key_number = character_to_value(generated_character)
    encrypted_number = character_to_value(character) + key_number
    encrypted_character = value_to_character(encrypted_number)
    return encrypted_character


def decrypt_character(character, card_deck):
    """
    This is the inverted version of encrypt_character() that takes in the encrypted character
    and the deck and encrypts the character. It first retrieves a character/value
    from the deck, converts that character/value and then subtracts it from the numerical
    representation of the encrypted character pass in. The new number is then converted back
    into a character and returned.
    """
    generated_character = retrieve_key(card_deck)
    key_number = character_to_value(generated_character)
    decrypted_number = character_to_value(character) - key_number
    decrypted_character = value_to_character(decrypted_number)
    return decrypted_character


def retrieve_key(card_deck):
    """
    This function retrieves a character from the deck. Reads the first card on the deck and
    transforms it to a numerical representation.
    """
    top_of_deck = card_deck[0]
    card_value = character_to_value(top_of_deck)
    index_value = ((card_value + 1) % len(card_deck))
    return card_deck[index_value]


def encrypt_message(user_message, card_deck):
    """
    This function iterates each character of the message passed in and
    calls the encrypt_character() function to encrypt each character
    separately. Encrypted character is added to a string and if the
    character is not an alphabet letter, it skips the encrypting.
    """
    encrypted_phrase = ''
    shuffled_deck = card_deck
    for letter in user_message:
        if ('A' <= letter <= 'Z') or ('a' <= letter <= 'z'):
            shuffled_deck = shuffle_deck(shuffled_deck)
            encrypted_character = encrypt_character(letter, shuffled_deck)
            encrypted_phrase += encrypted_character
        else:
            encrypted_phrase += letter
    return encrypted_phrase


def decrypt_message(user_message, card_deck):
    """
    This function iterates each character of the message passed in and
    calls the decrypt_character() function to decrypt each character
    separately. Decrypted character is added to a string and if the
    character is not an alphabet letter, it skips the encrypting.
    """
    decrypted_phrase = ''
    shuffled_deck = card_deck
    for letter in user_message:
        if ('A' <= letter <= 'Z') or ('a' <= letter <= 'z'):
            shuffled_deck = shuffle_deck(shuffled_deck)
            decrypted_character = decrypt_character(letter, shuffled_deck)
            decrypted_phrase += decrypted_character
        else:
            decrypted_phrase += letter
    return decrypted_phrase


def shuffle_deck(card_deck):
    """
    This function moves the first joker down one place, the second joker down two
    places. Then it does a triple cut based on where the jokers are. The first section
    are to the top of the first joker and the third section are to the bottom of the
    second joker. All cards between the jokers and including the jokers themselves are
    in the middle section. First and third section swap to create a new deck. Lastly,
    the function looks at the last card in the deck, converts it to its numerical
    representation and takes that many cards from the top of the deck and puts them
    right above the last card.
    """
    shuffled_deck = card_deck

    # Joker shifting
    for index in range(len(shuffled_deck)):
        if shuffled_deck[index] == '1':
            new_deck = ''
            zeroth_index = index
            first_index = (index + 1) % len(shuffled_deck)
            joker_value = shuffled_deck[zeroth_index]
            value1 = shuffled_deck[first_index]
            new_deck += shuffled_deck[:index]
            new_deck = new_deck + value1 + joker_value
            new_deck += shuffled_deck[(index + 2):]

            if new_deck[len(shuffled_deck) - 1] == '1':
                # This is to take care of when joker is last
                new_modified_deck = ''
                new_modified_deck = new_modified_deck + new_deck[0] + joker_value + new_deck[1:(len(new_deck) - 1)]
                new_deck = new_modified_deck
            shuffled_deck = new_deck
            break

    for index in range(len(shuffled_deck)):
        if shuffled_deck[index] == '2':
            new_deck = ''
            zeroth_index = index
            first_index = (index + 1) % len(shuffled_deck)
            second_index = (index + 2) % len(shuffled_deck)
            joker_value = shuffled_deck[zeroth_index]
            value1 = shuffled_deck[first_index]
            value2 = shuffled_deck[second_index]
            new_deck += shuffled_deck[:index]
            new_deck = new_deck + value1 + value2 + joker_value
            new_deck += shuffled_deck[(index + 3):]

            if new_deck[len(shuffled_deck) - 1] == '2':
                # This is to take care of when joker is last
                new_modified_deck = ''
                new_modified_deck = new_modified_deck + new_deck[0:2] + joker_value + new_deck[2:(len(new_deck) - 1)]
                new_deck = new_modified_deck
            if new_deck[len(shuffled_deck) - 2] == '2':
                # This is to take care of when joker is second to last
                new_modified_deck = ''
                new_modified_deck = new_modified_deck + new_deck[0:1] + joker_value + new_deck[1:(len(new_deck) - 1)]
                new_deck = new_modified_deck
            shuffled_deck = new_deck
            break

    # Triple cutting deck
    joker1_index = 0
    joker2_index = 0
    for index in range(len(shuffled_deck)):
        if shuffled_deck[index] == '1':
            joker1_index = index
        if shuffled_deck[index] == '2':
            joker2_index = index
    first_deck_section = shuffled_deck[:joker1_index]
    second_deck_section = shuffled_deck[joker1_index:(joker2_index + 1)]
    third_deck_section = shuffled_deck[(joker2_index + 1):]
    shuffled_deck = third_deck_section + second_deck_section + first_deck_section

    # Moving cards to bottom of deck. Count Cut
    bottom_card_value = shuffled_deck[len(shuffled_deck) - 1]
    if (bottom_card_value == 1) or (bottom_card_value == 2):
        # Do nothing if joker is at the bottom
        shuffled_deck = shuffled_deck
    elif (bottom_card_value != 1) and (bottom_card_value != 2):
        first_half_deck = shuffled_deck[:character_to_value(bottom_card_value)]
        second_half_deck = shuffled_deck[character_to_value(bottom_card_value):-1]
        last_card = bottom_card_value
        shuffled_deck = second_half_deck + first_half_deck + last_card
    return shuffled_deck


if __name__ == '__main__':
    user_input()
    if mode == 'e':
        print('Result: ' + encrypt_message(message, deck))
    if mode == 'd':
        print('Result: ' + decrypt_message(message, deck))